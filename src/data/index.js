export const BUTTONS = [
  {
    type: 'delete',
    value: '⌫',
    name: 'delete',
  },
  {
    type: 'operand',
    value: '%',
    name: 'percent',
  },
  {
    type: 'operand',
    value: '/',
    name: 'divide',
  },
  {
    type: 'operand',
    value: '*',
    name: 'multiply',
  },
  {
    type: 'number',
    value: '7',
    name: '7',
  },
  {
    type: 'number',
    value: '8',
    name: '8',
  },
  {
    type: 'number',
    value: '9',
    name: '9',
  },
  {
    type: 'operand',
    value: '-',
    name: 'subtract',
  },
  {
    type: 'number',
    value: '4',
    name: '4',
  },
  {
    type: 'number',
    value: '5',
    name: '5',
  },
  {
    type: 'number',
    value: '6',
    name: '6',
  },
  {
    type: 'operand',
    value: '+',
    name: 'add',
  },
  {
    type: 'number',
    value: '1',
    name: '1',
  },
  {
    type: 'number',
    value: '2',
    name: '2',
  },
  {
    type: 'number',
    value: '3',
    name: '3',
  },
  {
    type: 'clear',
    value: 'C',
    name: 'C',
  },
  {
    type: 'number',
    value: '0',
    name: '0',
  },
  {
    type: 'dot',
    value: '.',
    name: '.',
  },
  {
    type: 'equal',
    value: '=',
    name: 'equal',
  },
];
